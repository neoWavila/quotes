const API = 'https://quotes15.p.rapidapi.com/quotes/random/';
const OPTIONS = {
	method: 'GET',
	headers: {
		'X-RapidAPI-Key': '6e0ffc4b3cmsh6d6ff34ecb0f54dp12ca57jsn3aa2582f4f61',
		'X-RapidAPI-Host': 'quotes15.p.rapidapi.com'
	}
};

async function fetchData(url){
	try {
		const response = await fetch(url, OPTIONS);
		return await response.json();
	} catch (error) {
		return error;
	}
}

async function getEnglishQuote() {
	const quote = await fetchData(`${API}?language_code=en`);
	buildQuoteContent(quote);
}

function getSpanishQuote(){
	fetch(`${API}?language_code=es`, OPTIONS)
		.then(response => response.json())
		.then(quote => buildQuoteContent(quote))
		.catch(error => console.error(error))
		.finally(console.log('All ready 😁'));
}

function buildQuoteContent(quote){
	const EM_DASH = "—";
	const QUOTATION_MARKS = "\""
	document.getElementById('quote').textContent = `${QUOTATION_MARKS}${quote.content}${QUOTATION_MARKS}`;
	document.getElementById('origin').textContent = `${EM_DASH}${quote.originator.name}`;
}

document.addEventListener('DOMContentLoaded', function () {
  getEnglishQuote();

  const btnGenerateEn = document.getElementById('generate-en');
  const btnGenerateEs = document.getElementById('generate-es');

  btnGenerateEn.addEventListener('click', function () {
    getEnglishQuote();
  });

  btnGenerateEs.addEventListener('click', function () {
    getSpanishQuote();
  });
});